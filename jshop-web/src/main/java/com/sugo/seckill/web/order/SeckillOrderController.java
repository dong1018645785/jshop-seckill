package com.sugo.seckill.web.order;

import com.google.common.util.concurrent.RateLimiter;
import com.sugo.seckill.error.BaseException;
import com.sugo.seckill.http.HttpResult;
import com.sugo.seckill.http.HttpStatus;
import com.sugo.seckill.mq.MqProducer;
import com.sugo.seckill.order.service.SeckillOrderService;
import com.sugo.seckill.pojo.FrontUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.consumer.MQConsumer;
import org.apache.rocketmq.client.consumer.MQPullConsumer;
import org.apache.rocketmq.client.producer.MQProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.*;


/**
 * controller
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/seckill")
public class SeckillOrderController {

	@Autowired
	private SeckillOrderService seckillOrderService;


			/**
             * @Description: 获取时间
             * @Author: hubin
             * @CreateDate: 2020/6/10 16:19
             * @UpdateUser: hubin
             * @UpdateDate: 2020/6/10 16:19
             * @UpdateRemark: 修改内容
             * @Version: 1.0
             */
	@RequestMapping("/submitOrder/times")
	public HttpResult getConcurrentTime(){
		return HttpResult.ok(System.currentTimeMillis()+"");
	}


	@RequestMapping("/test")
	public HttpResult getSeckillGoods(Long seckillId){
		seckillOrderService.getSeckillGoods(seckillId);
		return HttpResult.ok();
	}

	/**
	 * @Description: 普通下单操作
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/{killId}/{token}")
	public HttpResult startKilled(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilled(killId, userId);

		return result;

	}

	/**
	 * @Description: mysql分布式锁：for update 悲观锁方式
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/sql/{killId}/{token}")
	public HttpResult startKilledByMySQLLock(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledByMySQLLock(killId,userId);

		return result;
	}

	/**
	 * @Description: mysql分布式锁：version 乐观锁的方式
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/version/{killId}/{token}")
	public HttpResult startKilledByLockVersion(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledByLockVersion(killId,userId);

		return result;
	}

	/**
	 * @Description: redis分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/redis/{killId}/{token}")
	public HttpResult startKilledByRedisLock(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledByRedisLock(killId,userId);

		return result;
	}


	/**
	 * @Description: zk分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/zk/{killId}/{token}")
	public HttpResult startKilledByZkLock(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledByZkLock(killId,userId);

		return result;
	}

	/**
	 * @Description: zk分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/cache/{killId}/{token}")
	public HttpResult startKilledByCache(@PathVariable Long killId, @PathVariable String token){
		//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";
		//下单
		HttpResult result = seckillOrderService.startKilledOrder(killId,userId);

		return result;
	}



	@Autowired
	private ExecutorService executorService;

	@Autowired
	private MqProducer producer;


	// 定义一个信号量的池子
	private final Semaphore permit = new Semaphore(10,true);


	@Autowired
	private RateLimiter rateLimiter;

	/**
	 * @Description: 下单业务优化实现,实现数据一致性
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@RequestMapping("/order/kill/cons/{killId}/{token}")
	public HttpResult startKilledConsitentOrder(@PathVariable Long killId, @PathVariable String token) throws BaseException{

		try {
		// 获取信号量
		//permit.acquire();
			boolean res = rateLimiter.tryAcquire();

			if(!res){
				return HttpResult.error("你被限流了！！！");
			}

			//判断
		if(StringUtils.isBlank(token)){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}
		//获取用户数据
		FrontUser user = seckillOrderService.getUserInfoFromRedis(token);

		//判断
		if(user == null){
			return HttpResult.error(HttpStatus.SC_EXPECTATION_FAILED,"用户未登录");
		}

		//获取userid
		String userId = user.getId()+"";

		// 线程同步调用方法，20个等待队列，泄洪
		// 发送消息
		Future<Object> future = executorService.submit(new Callable<Object>() {

			@Override
			public Object call() throws Exception {

				// 发送消息
				boolean res = producer.asncSendTransactionMsg(killId, userId);
				if(!res){
					throw new BaseException(HttpStatus.SC_EXPECTATION_FAILED,"发送消息失败");
				}


				return null;
			}
		});


			future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new BaseException(HttpStatus.SC_EXPECTATION_FAILED,"发送消息失败");
		} catch (ExecutionException e) {
			e.printStackTrace();
			throw new BaseException(HttpStatus.SC_EXPECTATION_FAILED,"发送消息失败");
		}finally {
			// 释放信号量
			permit.release();
		}

		return null;

	}


}

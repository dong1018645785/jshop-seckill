package com.sugo.seckill.config;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.util.concurrent.RateLimiter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName GuavaCacheConfig
 * @Description
 * @Author hubin
 * @Date 2020/8/29 21:27
 * @Version V1.0
 **/
@Configuration
public class GuavaCacheConfig {

    //定义cache对象
    private Cache<String,Object> guavaCahce = null;


    @PostConstruct
    public void init(){
        guavaCahce =  CacheBuilder.newBuilder()
                //设置缓存初始化容量
                .initialCapacity(10)
                //设置cache中最多可以存储多少个缓存（可以存储多个key）,超过100个，采用LRU内存淘汰策略进行淘汰
                .maximumSize(100)
                //缓存从写入开始计时，60s后过期
                .expireAfterWrite(60, TimeUnit.SECONDS)
                .build();



    }

    //把guavaCache交给spring容器进行管理
    @Bean
    public Cache<String,Object> getCahce(){
        return guavaCahce;
    }


}


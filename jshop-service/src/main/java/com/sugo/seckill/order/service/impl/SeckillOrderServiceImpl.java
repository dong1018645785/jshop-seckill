package com.sugo.seckill.order.service.impl;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.github.pagehelper.Page;
import com.sugo.seckill.aop.lock.ServiceLock;
import com.sugo.seckill.aop.redis.ServiceRedisLock;
import com.sugo.seckill.aop.zk.ServiceZkLock;
import com.sugo.seckill.distributedlock.redis.RedissLockUtil;
import com.sugo.seckill.distributedlock.zookeeper.ZkLockUtil;
import com.sugo.seckill.error.BaseException;
import com.sugo.seckill.http.HttpResult;
import com.sugo.seckill.http.HttpStatus;
import com.sugo.seckill.mapper.order.SeckillGoodsMapper;
import com.sugo.seckill.mapper.order.SeckillOrderMapper;
import com.sugo.seckill.mapper.pay.TbPayLogMapper;
import com.sugo.seckill.mq.MqProducer;
import com.sugo.seckill.order.service.SeckillOrderService;
import com.sugo.seckill.page.PageResult;
import com.sugo.seckill.pojo.*;
import com.sugo.seckill.queue.jvm.SeckillQueue;
import com.sugo.seckill.utils.IdWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import com.github.pagehelper.PageHelper;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import tk.mybatis.mapper.entity.Example;

/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class SeckillOrderServiceImpl implements SeckillOrderService {

	@Autowired
	private SeckillOrderMapper seckillOrderMapper;

	//注入支付
	@Autowired
	private TbPayLogMapper payLogMapper;


	//注入商品对象
	@Autowired
	private SeckillGoodsMapper seckillGoodsMapper;

	//注入redis
	@Autowired
	private RedisTemplate redisTemplate;

	//注入Idworker
	@Autowired
	private IdWorker idWorker;



	//程序锁
	//互斥锁 参数默认false，不公平锁
	private Lock lock = new ReentrantLock(true);

	//日志
	private final static Logger LOGGER = LoggerFactory.getLogger(SeckillOrderServiceImpl.class);



	// disruptor 队列 --- 600w /s
	// 分布式队列
	// redis
	// mq


	public void getSeckillGoods(Long seckillId){

		redisTemplate.delete("seckill_goods_"+seckillId);
		redisTemplate.delete("seckill_goods_stock_"+seckillId);

		//创建example对象
		Example example = new Example(TbSeckillGoods.class);
		Example.Criteria criteria = example.createCriteria();

		/*//设置查询条件
		//状态可用
		criteria.andEqualTo("status",1);
		//时间必须在活动区间
		criteria.andCondition("now() BETWEEN start_time_date AND end_time_date");
		//库存必须大于0
		criteria.andGreaterThan("stockCount",0);
		criteria.andEqualTo("id",1);

		//获取Redis中所有的key,实现排除当前Redis中已经存在的商品
		Set<Long> ids = redisTemplate.boundHashOps("seckillGoods").keys();
		//判断
		if(ids!=null && ids.size()>0){
			criteria.andNotIn("id",ids);
		}

		//查询
		List<TbSeckillGoods> seckillGoodsList = seckillGoodsMapper.selectByExample(example);
*/
		TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(seckillId);

		//判断是否有入库商品
		//if(seckillGoodsList!=null && seckillGoodsList.size()>0){
			//循环
			//for (TbSeckillGoods goods : seckillGoodsList) {

				//放入商品对象
				redisTemplate.opsForValue().set("seckill_goods_"+seckillId,seckillGoods);
				//存储库存
				redisTemplate.opsForValue().set("seckill_goods_stock_"+seckillId,seckillGoods.getStockCount());

               /* //存储到redis
                redisTemplate.boundHashOps("seckillGoods").put(String.valueOf(goods.getId()),goods);
                //存储库存:剩余库存存，用来防止超卖
                redisTemplate.boundHashOps("seckillGoodsCount").put(String.valueOf(goods.getId()),goods.getStockCount());*/

			//}
		//}
	}

	/**
	 * @Description: 普通下单操作 --- 使用lock锁的方式控制库存
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceLock
	@Override
	public HttpResult startKilled(Long killId, String userId) {

		try {

			//实现一个加锁的动作
			//lock.lock();

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
                return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
            }
			if(seckillGoods.getStatus() != 1){
                return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
            }
			if(seckillGoods.getStockCount() <= 0){
                return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
            }
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
                return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
            }
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
                return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
            }

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally {
			// 防止死锁，必须业务结束后，释放锁
			lock.unlock();
		}*/
		return null;
	}

	/**
	 * @Description: mysql分布式锁：for update 悲观锁方式
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@Override
	public HttpResult startKilledByMySQLLock(Long killId, String userId) {

		try {

			//实现一个加锁的动作
			//lock.lock();

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKeyBySQLLock(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally {
			// 防止死锁，必须业务结束后，释放锁
			lock.unlock();
		}*/
		return null;
	}

	/**
	 * @Description: mysql分布式锁：version乐观锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@Override
	public HttpResult startKilledByLockVersion(Long killId, String userId) {
		try {

			//实现一个加锁的动作
			//lock.lock();

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);

			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			// 乐观锁的方式
			int count = seckillGoodsMapper.updateSeckillGoodsByPrimaryKeyByVersion(killId, seckillGoods.getVersion());

			//判断
			if(count <= 0){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"没有获得执行权限");
			}


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");
		} catch (Exception e) {
			e.printStackTrace();
		}/*finally {
			// 防止死锁，必须业务结束后，释放锁
			lock.unlock();
		}*/
		return null;
	}


	/**
	 * @Description: redis分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceRedisLock
	@Override
	public HttpResult startKilledByRedisLock(Long killId, String userId) {
		//实现一个加锁的动作
		//boolean succ = RedissLockUtil.tryLock("seckill_redis_lock_" + killId, TimeUnit.SECONDS, 3, 10);
		try {

			//if(succ){

			// 从数据库查询商品数据
			TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
			//判断
			if(seckillGoods == null){
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS,"商品不存在");
			}
			if(seckillGoods.getStatus() != 1){
				return HttpResult.error(HttpStatus.SEC_NOT_UP,"商品未审核");
			}
			if(seckillGoods.getStockCount() <= 0){
				return HttpResult.error(HttpStatus.SEC_GOODS_END,"商品已售罄");
			}
			if(seckillGoods.getStartTimeDate().getTime() > new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START,"活动未开始");
			}
			if(seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()){
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END,"活动结束");
			}

			//库存扣减
			seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			//保存订单
			seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");

			//}

		} catch (Exception e) {
			e.printStackTrace();
		}/*finally {
			if(succ){
				// 释放锁
				RedissLockUtil.unlock("seckill_redis_lock_" + killId);
			}

		}*/
		return null;
	}


	/**
	 * @Description: zk分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Transactional
	@ServiceZkLock
	@Override
	public HttpResult startKilledByZkLock(Long killId, String userId) {

		// 获取锁
		//boolean res = ZkLockUtil.acquire(3, TimeUnit.SECONDS);
		try {

			//实现一个加锁的动作
			//if(res) {

				// 从数据库查询商品数据
				TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);
				//判断
				if (seckillGoods == null) {
					return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS, "商品不存在");
				}
				if (seckillGoods.getStatus() != 1) {
					return HttpResult.error(HttpStatus.SEC_NOT_UP, "商品未审核");
				}
				if (seckillGoods.getStockCount() <= 0) {
					return HttpResult.error(HttpStatus.SEC_GOODS_END, "商品已售罄");
				}
				if (seckillGoods.getStartTimeDate().getTime() > new Date().getTime()) {
					return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START, "活动未开始");
				}
				if (seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()) {
					return HttpResult.error(HttpStatus.SEC_ACTIVE_END, "活动结束");
				}

				//库存扣减
				seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
				//更新库存
				seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


				//下单
				TbSeckillOrder order = new TbSeckillOrder();
				order.setSeckillId(killId);
				order.setUserId(userId);
				order.setCreateTime(new Date());
				order.setStatus("0");
				order.setMoney(seckillGoods.getCostPrice());

				//保存订单
				seckillOrderMapper.insertSelective(order);

				return HttpResult.ok("秒杀成功");

		} catch (Exception e) {
			e.printStackTrace();
		}/*finally {
			// 如果锁还存在
			if(res){
				ZkLockUtil.release();
			}*/

		return null;
	}

	/**
	 * @Description: 下单业务优化实现（写操作优化）
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public HttpResult startKilledOrder(Long killId, String userId) {

		try {


			// 从数据库查询商品数据
			//TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);

			// 第一个优化动作：读缓存
			// 从缓存中查询数据
			TbSeckillGoods seckillGoods = (TbSeckillGoods) redisTemplate.opsForValue().get("seckill_goods_"+killId);

			//判断
			if (seckillGoods == null) {
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS, "商品不存在");
			}
			if (seckillGoods.getStatus() != 1) {
				return HttpResult.error(HttpStatus.SEC_NOT_UP, "商品未审核");
			}
			if (seckillGoods.getStockCount() <= 0) {
				return HttpResult.error(HttpStatus.SEC_GOODS_END, "商品已售罄");
			}
			if (seckillGoods.getStartTimeDate().getTime() > new Date().getTime()) {
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START, "活动未开始");
			}
			if (seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()) {
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END, "活动结束");
			}

			//库存扣减
			//seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			//seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			boolean res = this.reduceStock(killId);
			if(!res){
				return HttpResult.error("扣减库存失败");
			}


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			// 入队操作，下单成功
			Boolean produce = SeckillQueue.getMailQueue().produce(order);

			if(!produce){
				return HttpResult.error("秒杀失败");
			}

			//保存订单
			//seckillOrderMapper.insertSelective(order);

			return HttpResult.ok("秒杀成功");

			/*TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public void afterCommit() {
					super.afterCommit();
				}
			});*/

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}


	/**
	 * @Description: 下单业务优化实现,实现数据一致性
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public HttpResult startKilledConsitentOrder(Long killId, String userId) throws BaseException {

		try {


			// 从数据库查询商品数据
			//TbSeckillGoods seckillGoods = seckillGoodsMapper.selectByPrimaryKey(killId);

			// 第一个优化动作：读缓存
			// 从缓存中查询数据
			TbSeckillGoods seckillGoods = (TbSeckillGoods) redisTemplate.opsForValue().get("seckill_goods_"+killId);

			//判断
			if (seckillGoods == null) {
				return HttpResult.error(HttpStatus.SEC_GOODS_NOT_EXSISTS, "商品不存在");
			}
			if (seckillGoods.getStatus() != 1) {
				return HttpResult.error(HttpStatus.SEC_NOT_UP, "商品未审核");
			}
			if (seckillGoods.getStockCount() <= 0) {
				return HttpResult.error(HttpStatus.SEC_GOODS_END, "商品已售罄");
			}
			if (seckillGoods.getStartTimeDate().getTime() > new Date().getTime()) {
				return HttpResult.error(HttpStatus.SEC_ACTIVE_NOT_START, "活动未开始");
			}
			if (seckillGoods.getEndTimeDate().getTime() <= new Date().getTime()) {
				return HttpResult.error(HttpStatus.SEC_ACTIVE_END, "活动结束");
			}

			//库存扣减
			//seckillGoods.setStockCount(seckillGoods.getStockCount() - 1);
			//更新库存
			//seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			// 不能在此处发送消息
			boolean res = this.reduceStock(killId);
			if(!res){
				throw new BaseException(HttpStatus.SEC_GOODS_STOCK_FAIL,"发送消息失败");
			}


			//下单
			TbSeckillOrder order = new TbSeckillOrder();
			order.setSeckillId(killId);
			order.setUserId(userId);
			order.setCreateTime(new Date());
			order.setStatus("0");
			order.setMoney(seckillGoods.getCostPrice());

			// 入队操作，下单成功
			/*Boolean produce = SeckillQueue.getMailQueue().produce(order);

			if(!produce){
				return HttpResult.error("秒杀失败");
			}*/

			//保存订单
			seckillOrderMapper.insertSelective(order);


			// 设置事务状态
			seckillGoods.setStockCount(null);
			seckillGoods.setTransactionStatus(1);
			//更新事务状态
			seckillGoodsMapper.updateByPrimaryKeySelective(seckillGoods);


			return HttpResult.ok("秒杀成功");

			/*TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public void afterCommit() {
					super.afterCommit();
				}
			});*/

		} catch (Exception e) {
			e.printStackTrace();
			throw new BaseException(HttpStatus.SEC_GOODS_STOCK_FAIL,"下单失败");
		}

	}


	// 注入消息生产者
	@Autowired
	private MqProducer producer;


	/**
	 * @Description: 扣减redis库存
	 * @Author: hubin
	 * @CreateDate: 2020/12/26 21:21
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/12/26 21:21
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	private boolean reduceStock(Long killId) {
		// 直接扣减redis库存，利用redis原子性操作
		Long res = redisTemplate.opsForValue().increment("seckill_goods_stock_" + killId, -1);
		if(res >=0){
			//发送消息
		/*	boolean succ = producer.asncSendMsg(killId);
			if(!succ){
				return false;
			}
         */
			return true;
		}else{
			// 失败，库存回滚
			redisTemplate.opsForValue().increment("seckill_goods_stock_" + killId, 1);
			return false;
		}
	}

	/**
	 * @Description: 真正的下单处理
	 * @Author: hubin
	 * @CreateDate: 2020/11/30 21:20
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/30 21:20
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public void startSubmitOrder(TbSeckillOrder seckillOrder) {

		// 下单落库
		seckillOrderMapper.insertSelective(seckillOrder);

	}


	/**
	 * 按分页查询
	 */
	@Override
	@Transactional
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		Page<TbSeckillOrder> page=   (Page<TbSeckillOrder>) seckillOrderMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}


	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	@Override
	@Transactional
	public TbSeckillOrder findOne(Long id){
		return seckillOrderMapper.selectByPrimaryKey(id);
	}

	/**
	 * @Description: 支付完毕，更新订单的状态
	 * @Author: hubin
	 * @CreateDate: 2020/6/10 22:20
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/6/10 22:20
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	@Transactional
	public void updateOrderStatus(String out_trade_no, String transaction_id) {
		//1.修改支付日志状态

		TbPayLog payLog = payLogMapper.selectByPrimaryKey(out_trade_no);
		payLog.setPayTime(new Date());//支付时间
		payLog.setTradeState("1");//交易状态
		payLog.setTransactionId(transaction_id);//流水号
		payLogMapper.updateByPrimaryKey(payLog);

		//2.修改订单状态
		String orderList = payLog.getOrderList();
		String[] ids = orderList.split(",");//订单号
		for(String id:ids){

			TbSeckillOrder order = seckillOrderMapper.selectByPrimaryKey( Long.valueOf(id) );
			order.setStatus("2");//支付状态
			order.setPayTime(new Date());//支付时间
			seckillOrderMapper.updateByPrimaryKey(order);
		}
	}


	@Override
	public TbPayLog searchPayLogFromRedis(String userId) {
		return (TbPayLog) redisTemplate.boundHashOps("payLog").get(userId);
	}

	/**
	 * @Description: 根据token查询用户信息
	 * @Author: hubin
	 * @CreateDate: 2020/6/10 10:47
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/6/10 10:47
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	@Override
	public FrontUser getUserInfoFromRedis(String token) {
		FrontUser user =
				(FrontUser) redisTemplate.opsForValue().get(token);
		return user;
	}

	@Override
	public HttpResult getOrderMoney(Long orderId) {
		TbSeckillOrder order = seckillOrderMapper.selectByPrimaryKey(orderId);
		HttpResult httpResult = new HttpResult();
		httpResult.setData(order.getMoney());
		return httpResult;
	}

	@Override
	public TbSeckillOrder findOrderById(Long orderId) {
		return seckillOrderMapper.selectByPrimaryKey(orderId);
	}
}

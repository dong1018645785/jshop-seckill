package com.sugo.seckill.order.service;

import com.sugo.seckill.error.BaseException;
import com.sugo.seckill.http.HttpResult;
import com.sugo.seckill.page.PageResult;
import com.sugo.seckill.pojo.*;

/**
 * 服务层接口
 * @author Administrator
 */
public interface SeckillOrderService {

	/**
	 * 返回分页列表
	 * @return
	 */
	public PageResult findPage(int pageNum, int pageSize);
	

	/**
	 * 根据ID获取实体
	 * @param id
	 * @return
	 */
	public TbSeckillOrder findOne(Long id);

	public void updateOrderStatus(String out_trade_no, String transaction_id);


	public TbPayLog searchPayLogFromRedis(String userId);


	/**
	 * @Description: 从redis中查询用户信息
	 * @Author: hubin
	 * @CreateDate: 2020/6/10 10:47
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/6/10 10:47
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	FrontUser getUserInfoFromRedis(String token);

    HttpResult getOrderMoney(Long orderId);

	TbSeckillOrder findOrderById(Long orderId);

	public void getSeckillGoods(Long seckillId);




	/**
	 * @Description: 普通下单操作
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilled(Long killId,String userId);

	/**
	 * @Description: mysql分布式锁：for update 悲观锁方式
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledByMySQLLock(Long killId,String userId);


	/**
	 * @Description: mysql分布式锁：version乐观锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledByLockVersion(Long killId,String userId);



	/**
	 * @Description: redis分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledByRedisLock(Long killId,String userId);

	/**
	 * @Description: zk分布式锁
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledByZkLock(Long killId,String userId);


	/**
	 * @Description: 下单业务优化实现（写操作优化）
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledOrder(Long killId,String userId);


	/**
	 * @Description: 下单业务优化实现,实现数据一致性
	 * @Author: hubin
	 * @CreateDate: 2020/11/27 22:01
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/27 22:01
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	public HttpResult startKilledConsitentOrder(Long killId, String userId)  throws BaseException;

	/**
	 * @Description: 真正的下单处理
	 * @Author: hubin
	 * @CreateDate: 2020/11/30 21:20
	 * @UpdateUser: hubin
	 * @UpdateDate: 2020/11/30 21:20
	 * @UpdateRemark: 修改内容
	 * @Version: 1.0
	 */
	void startSubmitOrder(TbSeckillOrder seckillOrder);
}

package com.sugo.seckill.aop.zk;

import java.lang.annotation.*;

/**
 * @ClassName ServiceRedisLock
 * @Description 自定义注解，实现锁上移
 * @Author hubin
 * @Date 2020/12/24 22:06
 * @Version V1.0
 **/
@Target({ElementType.PARAMETER,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceZkLock {
    String description() default "";
}

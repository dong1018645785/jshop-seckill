package com.sugo.seckill.aop.zk;

import com.sugo.seckill.distributedlock.zookeeper.ZkLockUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName LockAspect
 * @Description
 * @Author hubin
 * @Date 2020/12/24 22:07
 * @Version V1.0
 **/
@Component
@Scope
@Aspect
@Order(1)
public class LockZkAspect {


    //创建lock锁的对象


    // service 切入点
    @Pointcut("@annotation(com.sugo.seckill.aop.zk.ServiceZkLock)")
    public void lockAspect(){

    }

    @Around("lockAspect()")
    public Object around(ProceedingJoinPoint joinPoint){

        // 加锁动作
        boolean res = ZkLockUtil.acquire(3, TimeUnit.SECONDS);

        Object obj = null;
        try {
            if(res) {
                // 业务执行地方
                obj = joinPoint.proceed();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            // 业务执行结束，必须释放锁
           if(res){
               ZkLockUtil.release();
           }
        }

        return obj;
    }




}











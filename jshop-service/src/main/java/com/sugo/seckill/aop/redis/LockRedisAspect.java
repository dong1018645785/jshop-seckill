package com.sugo.seckill.aop.redis;

import com.sugo.seckill.distributedlock.redis.RedissLockUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName LockAspect
 * @Description
 * @Author hubin
 * @Date 2020/12/24 22:07
 * @Version V1.0
 **/
@Component
@Scope
@Aspect
@Order(1)
public class LockRedisAspect {


    @Autowired
    private HttpServletRequest request;


    // service 切入点
    @Pointcut("@annotation(com.sugo.seckill.aop.redis.ServiceRedisLock)")
    public void lockAspect(){

    }

    @Around("lockAspect()")
    public Object around(ProceedingJoinPoint joinPoint){

        //获取请求地址
        String requestURI = request.getRequestURI();
        // 获取参数
        String killId = requestURI.substring(requestURI.lastIndexOf("/") - 1,
                requestURI.lastIndexOf("/"));

        // 加锁动作
        boolean res = RedissLockUtil.tryLock("seckill_redis_lock_" + killId,
                TimeUnit.SECONDS,
                3,
                10);

        Object obj = null;
        try {
            // 加锁成功，才能执行业务
            if(res) {
                // 业务执行地方
                obj = joinPoint.proceed();
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            // 业务执行结束，必须释放锁
            if(res){
                RedissLockUtil.unlock("seckill_redis_lock_" + killId);
            }
        }

        return obj;
    }




}










